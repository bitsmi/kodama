package com.bitsmi.kodama.search;

public class ElasticErrorConstants 
{
	/* GENERICS */
	/** [ELA-0001] S'ha produit un error no especificat: &lt;Missatge d'error&gt; */
	public static final String ERROR_CODE_ELA_0001 = "ELA-0001";
	public static final String ERROR_DESC_ELA_0001 = "S'ha produit un error no especificat: %1$s";
	
	/* QUERIES */
	/** [ELA-3001] No s'ha especificat una consulta de cerca vàlida [&lt;consulta&gt;] */
	public static final String ERROR_CODE_ELA_3001 = "ELA-3001";
	public static final String ERROR_DESC_ELA_3001 = "No s'ha especificat una consulta de cerca vàlida [%1$s]";
}
