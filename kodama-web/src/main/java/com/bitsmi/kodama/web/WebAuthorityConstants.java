package com.bitsmi.kodama.web;

public class WebAuthorityConstants 
{
	public static final String AUTH_CORE_USER_VIEW_PROFILE = "core.user.view.profile";
	
	public static final String AUTH_CORE_SQL_VIEW_MAIN = "core.sql.view.main";
}
