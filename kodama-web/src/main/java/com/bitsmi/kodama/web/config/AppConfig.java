package com.bitsmi.kodama.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableAsync
public class AppConfig extends WebMvcConfigurerAdapter
{
	/**
	 * Especifica el timeout dels events async (180 seg.)
	 */
	@Override
    public void configureAsyncSupport(AsyncSupportConfigurer configurer) 
	{
        configurer.setDefaultTimeout(180000);
    }
}
