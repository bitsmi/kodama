package com.bitsmi.kodama.web.controller;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
@RequestMapping("/ajax/application")
public class ApplicationAjaxController 
{
	@RequestMapping("/version")
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	public String getVersion() 
	{
		Package pkg = ApplicationAjaxController.class.getPackage();
		String version = null;
		if(pkg!=null){
			version = pkg.getImplementationVersion();
		}
		return (version!=null ? version : "Unknown Version");
	}
}
