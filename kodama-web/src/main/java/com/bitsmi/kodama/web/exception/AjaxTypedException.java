package com.bitsmi.kodama.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.bitsmi.kodama.spi.exception.TypedException;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class AjaxTypedException extends TypedException
{
	private static final long serialVersionUID = 1087630262000352325L;
	
	public AjaxTypedException(String errorCode) 
	{
		super(errorCode);
	}

	public AjaxTypedException(String errorCode, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) 
	{
		super(errorCode, message, cause, enableSuppression, writableStackTrace);
	}

	public AjaxTypedException(String errorCode, String message, Throwable cause) 
	{
		super(errorCode, message, cause);
	}

	public AjaxTypedException(String errorCode, String message) 
	{
		super(errorCode, message);
	}

	public AjaxTypedException(String errorCode, Throwable cause) 
	{
		super(errorCode, cause);
	}
}
