package com.bitsmi.kodama.web.exception;

import com.bitsmi.kodama.spi.exception.TypedException;

public class WebTypedException extends TypedException
{
	private static final long serialVersionUID = 1087630262000352325L;
	
	private boolean fragment = false;
	
	public WebTypedException(String errorCode) 
	{
		super(errorCode);
	}

	public WebTypedException(String errorCode, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) 
	{
		super(errorCode, message, cause, enableSuppression, writableStackTrace);
	}

	public WebTypedException(String errorCode, String message, Throwable cause) 
	{
		super(errorCode, message, cause);
	}

	public WebTypedException(String errorCode, String message) 
	{
		super(errorCode, message);
	}

	public WebTypedException(String errorCode, Throwable cause) 
	{
		super(errorCode, cause);
	}

	public boolean isFragment()
	{
		return fragment;
	}
	
	public WebTypedException setFragment(boolean fragment)
	{
		this.fragment = fragment;
		return this;
	}
}
