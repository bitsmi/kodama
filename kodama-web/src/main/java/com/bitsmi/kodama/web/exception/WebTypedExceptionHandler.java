package com.bitsmi.kodama.web.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class WebTypedExceptionHandler
{
	private final Logger log = LoggerFactory.getLogger(WebTypedExceptionHandler.class);
	
    @ExceptionHandler(WebTypedException.class)
    public ModelAndView handleConflict(HttpServletRequest request, HttpServletResponse response, WebTypedException e) throws Exception 
    {
    	log.error(e.getMessage(), e);
    	
    	ModelAndView mav = new ModelAndView();
        mav.addObject("errCode", e.getErrorCode());
        mav.addObject("errMsg", e.getMessage());
        mav.setViewName(e.isFragment() ? "common/error :: errorFragment" 
        		: "common/error :: errorFull");
        
        return mav;
    }
}