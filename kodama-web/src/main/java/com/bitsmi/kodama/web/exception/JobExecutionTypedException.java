package com.bitsmi.kodama.web.exception;

import org.quartz.JobExecutionException;

public class JobExecutionTypedException extends JobExecutionException 
{
	private static final long serialVersionUID = -9190229051174963451L;
	
	private String errorCode;
	
	public JobExecutionTypedException(String errorCode) 
	{
		super();
		this.errorCode = errorCode;
	}

	public JobExecutionTypedException(String errorCode, String message, Throwable cause) 
	{
		super(message, cause);
		this.errorCode = errorCode;
	}

	public JobExecutionTypedException(String errorCode, String message) 
	{
		super(message);
		this.errorCode = errorCode;
	}

	public JobExecutionTypedException(String errorCode, Throwable cause) 
	{
		super(cause);
		this.errorCode = errorCode;
	}

	public String getErrorCode() 
	{
		return errorCode;
	}

	public void setErrorCode(String errorCode) 
	{
		this.errorCode = errorCode;
	}
}
