package com.bitsmi.kodama.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.bitsmi.kodama.web.event.SpringBootFailedEventListener;
import com.bitsmi.kodama.web.event.SpringBootStartedEventListener;

// @SpringBootApplication == { @Configuration @EnableAutoConfiguration @ComponentScan }
@SpringBootApplication
@ComponentScan({
	"com.bitsmi.kodama.web",
	"com.bitsmi.kodama.core"
})
@EnableJpaRepositories({
	"com.bitsmi.kodama.core.repository"
})
@EntityScan({
	"com.bitsmi.kodama.core.domain"
})
@PropertySource("file:conf/application.properties")
public class MainProgram 
{
	public static void main(String...args) throws Exception
	{
		SpringApplication application = new SpringApplication(MainProgram.class);
		application.addListeners(new SpringBootFailedEventListener(),
				new SpringBootStartedEventListener()
		);
		application.run(args);
	}
}
