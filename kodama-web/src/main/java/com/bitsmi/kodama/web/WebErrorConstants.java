package com.bitsmi.kodama.web;

public class WebErrorConstants 
{
	/** [AJAX-0001] S'ha produit un error no especificat: <causa error> */
	public static final String ERROR_CODE_AJAX_0001 = "AJAX-0001";
	public static final String ERROR_DESC_AJAX_0001 = "S'ha produit un error no especificat: %1$s";
	
	/** [AJAX-0002] Error redirigint petició (<nova url>) */
	public static final String ERROR_CODE_AJAX_0002 = "AJAX-0002";
	public static final String ERROR_DESC_AJAX_0002 = "Error redirigint petició (%1$s)";
	
	/** [AJAX-0003] Accés no autoritzat (<url>) */
	public static final String ERROR_CODE_AJAX_0003 = "AJAX-0003";
	public static final String ERROR_DESC_AJAX_0003 = "Accés no autoritzat (%1$s)";
	
	public static final String ERROR_CODE_WEB_0001 = "WEB-0001";
	public static final String ERROR_DESC_WEB_0001 = "S'ha produit un error no especificat: %1$s";
}
