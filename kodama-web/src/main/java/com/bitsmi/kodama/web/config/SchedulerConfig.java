package com.bitsmi.kodama.web.config;

import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobListener;
import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;
import com.bitsmi.kodama.web.AppConstants;
import com.bitsmi.kodama.web.AppErrorConstants;
import com.bitsmi.kodama.web.exception.JobExecutionTypedException;
import com.bitsmi.kodama.web.util.AutoWiringSpringBeanJobFactory;

@Configuration
// La configuració només serà activa si la propietat està present a la configuració i == true
@ConditionalOnProperty(name = "scheduler.enabled")
public class SchedulerConfig 
{
	private final Logger logScheduler = LoggerFactory.getLogger(AppConstants.LOGGER_SCHEDULER);
	
	@Autowired
	private ApplicationContext applicationContext;
	
	@Bean
	public SchedulerFactoryBean scheduler(Trigger trigger, JobDetail job) 
	{
	    SchedulerFactoryBean schedulerFactory = new SchedulerFactoryBean();
	    schedulerFactory.setConfigLocation(new ClassPathResource("quartz.properties"));
	 
	    schedulerFactory.setJobFactory(springBeanJobFactory());
	    schedulerFactory.setJobDetails(job);
	    schedulerFactory.setTriggers(trigger);
	    schedulerFactory.setGlobalJobListeners(new JobListener()
	    {
			@Override
			public String getName() 
			{
				return "KODAMA_JOBS_ERROR_LISTENER";
			}

			@Override
			public void jobExecutionVetoed(JobExecutionContext arg0) 
			{
				// Res a fer...
			}

			@Override
			public void jobToBeExecuted(JobExecutionContext arg0) 
			{
				// Res a fer...
			}

			@Override
			public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) 
			{
				String jobName = context.getJobDetail().getKey().toString();
				logScheduler.info("Job :: " + jobName + " finalitzat");

				if(jobException!=null){
					if(jobException instanceof JobExecutionTypedException){
						JobExecutionTypedException typedException = (JobExecutionTypedException)jobException;
						logScheduler.error("[" + typedException.getErrorCode() + "] - " + typedException.getMessage());
					}
					else{
						// [CORE-8001] S'ha produit un error no especificat durant l'execució del job (<nom job>): <missatge error> 
		    			String errMsg = String.format(AppErrorConstants.ERROR_DESC_APP_1001, jobName, jobException.getMessage());
		    			logScheduler.error("[" + AppErrorConstants.ERROR_CODE_APP_1001 + "] - " + errMsg);
					}
				}
			}
	    	
	    });
	    return schedulerFactory;
	}
	
	@Bean
	public SpringBeanJobFactory springBeanJobFactory() 
	{
	    AutoWiringSpringBeanJobFactory jobFactory = new AutoWiringSpringBeanJobFactory();
	    jobFactory.setApplicationContext(applicationContext);
	    
	    return jobFactory;
	}
}
