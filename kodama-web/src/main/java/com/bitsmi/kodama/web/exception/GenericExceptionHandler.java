package com.bitsmi.kodama.web.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import com.bitsmi.kodama.web.WebErrorConstants;

@ControllerAdvice
@Order(Ordered.LOWEST_PRECEDENCE)
public class GenericExceptionHandler
{
	private final Logger log = LoggerFactory.getLogger(GenericExceptionHandler.class);
	
    @ExceptionHandler(Exception.class)
    public void handleConflict(HttpServletRequest request, HttpServletResponse response, Exception e) throws Exception 
    {
    	// [AJAX-0001] S'ha produit un error no especificat
    	String errMsg = "[" + WebErrorConstants.ERROR_CODE_AJAX_0001 + "] " + String.format(WebErrorConstants.ERROR_DESC_AJAX_0001, e.getMessage());
    	log.error(errMsg, e);
    	
    	response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
    	response.setCharacterEncoding("UTF-8");
    	response.getWriter().println(errMsg);
    }
}