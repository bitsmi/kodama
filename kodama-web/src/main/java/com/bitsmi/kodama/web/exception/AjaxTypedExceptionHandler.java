package com.bitsmi.kodama.web.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.bitsmi.kodama.spi.exception.TypedException;
import com.bitsmi.kodama.spi.exception.TypedRuntimeException;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE+1)
public class AjaxTypedExceptionHandler
{
	private final Logger log = LoggerFactory.getLogger(AjaxTypedExceptionHandler.class);
	
    @ExceptionHandler({TypedException.class, TypedRuntimeException.class})
    public void handleConflict(HttpServletRequest request, HttpServletResponse response, TypedException e) throws Exception 
    {
    	log.error(e.getMessage(), e);
    	
    	ResponseStatus status = AnnotationUtils.findAnnotation(e.getClass(), ResponseStatus.class);
    	response.setCharacterEncoding("UTF-8");
    	if(status==null){
    		response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
            response.getWriter().println(e.getMessage());
    	}
    	else{
    		response.setStatus(status.value().value());
    		response.getWriter().println("[" + e.getErrorCode() + "] - " + e.getMessage());
    	}
    }
}