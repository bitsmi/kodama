package com.bitsmi.kodama.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ApplicationController 
{
	@RequestMapping({"/", "/index"})
	public String index(Model model) 
	{
		return "index";
	}
	
	@RequestMapping("/login")
	public String login(Model model) 
	{
		return "login";
	}
	
	@RequestMapping("/session/expired")
	public String sessionExpired(Model model) 
	{
		model.addAttribute("warnMsg", "La sessió ha expirat. Refresqui la pantalla per continuar");
		
		return "common/error :: warnFragment";
	}
}
