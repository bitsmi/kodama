package com.bitsmi.kodama.web.dto;

public class MultisearchFilterDto 
{
	private String type;
	private String command;
	private String url;
	private String typeDesc;
	private String description;
	private String value;
	
	public String getType() 
	{
		return type;
	}
	
	public MultisearchFilterDto setType(String type) 
	{
		this.type = type;
		return this;
	}
	
	public String getCommand() 
	{
		return command;
	}
	
	public MultisearchFilterDto setCommand(String command) 
	{
		this.command = command;
		return this;
	}
	
	public String getUrl() 
	{
		return url;
	}
	
	public MultisearchFilterDto setUrl(String url) 
	{
		this.url = url;
		return this;
	}
	
	public String getTypeDesc() 
	{
		return typeDesc;
	}
	
	public MultisearchFilterDto setTypeDesc(String typeDesc) 
	{
		this.typeDesc = typeDesc;
		return this;
	}
	
	public String getDescription() 
	{
		return description;
	}
	
	public MultisearchFilterDto setDescription(String description) 
	{
		this.description = description;
		return this;
	}
	
	public String getValue()
	{
		return value;
	}
	
	public MultisearchFilterDto setValue(String value)
	{
		this.value = value;
		return this;
	}
}
