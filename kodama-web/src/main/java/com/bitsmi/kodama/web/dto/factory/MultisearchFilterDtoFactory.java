package com.bitsmi.kodama.web.dto.factory;

import com.bitsmi.kodama.web.dto.MultisearchFilterDto;

public class MultisearchFilterDtoFactory 
{
	public static MultisearchFilterDto newBooleanFilter(String command, String description, String value)
	{
		return new MultisearchFilterDto()
				.setType("BOOLEAN")
				.setTypeDesc("Booleà")
				.setCommand(command)
				.setDescription(description)
				.setValue(value);
	}
}
