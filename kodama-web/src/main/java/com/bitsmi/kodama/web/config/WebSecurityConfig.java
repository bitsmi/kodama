package com.bitsmi.kodama.web.config;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.DefaultRedirectStrategy;
import com.bitsmi.kodama.web.WebErrorConstants;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter 
{
	private final Logger log = LoggerFactory.getLogger(WebSecurityConfig.class);
	
	@Autowired
	private DataSource dataSource;
	
    @Override
    protected void configure(HttpSecurity http) throws Exception 
    {
        http.authorizeRequests()
        		.antMatchers("/css/**", 
    				"/js/**", 
    				"/fonts/**",
    				"/img/**"
        		).permitAll()
        		.anyRequest().authenticated()
        		.and()
        			.exceptionHandling().authenticationEntryPoint(authenticationEntryPoint())
        		.and()
        			.formLogin()
        				.loginPage("/login")
        				.permitAll()
    			.and()
    				.logout()
    				.permitAll();
    }

    @Bean
    public AuthenticationEntryPoint authenticationEntryPoint()
    {
    	return new AuthenticationEntryPoint() 
    	{
			@Override
			public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException 
			{
				String requestURI = request.getRequestURI();
				if(requestURI.contains("/fragment/")){
					new DefaultRedirectStrategy().sendRedirect(request, response, "/session/expired");
				}
				else if(requestURI.contains("/ajax/")){
					// [AJAX-0003] Accés no autoritzat
			    	String errMsg = "[" + WebErrorConstants.ERROR_CODE_AJAX_0003 + "] " + String.format(WebErrorConstants.ERROR_DESC_AJAX_0003, requestURI);
			    	log.error(errMsg);
			    	
			    	response.setStatus(HttpStatus.FORBIDDEN.value());
			    	response.setCharacterEncoding("UTF-8");
			    	response.getWriter().println(errMsg);
				}
				else{
				    new DefaultRedirectStrategy().sendRedirect(request, response, "/login");
			    }
			}
		};
    }
    
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception 
    {
    	String usersQuery = "select username, password, active as enabled "
    			+ "from app_user where username = ?";
    	
    	String authoritiesQuery = "select username, username from app_user where username = ? and 0=1";
    	
    	String groupAuthoritiesQuery = "select g.id, "
    			+ 	"g.name as group_name, "
    			+ 	"a.name as authority "
    			+ "from app_user u, "
    			+ 	"app_user_group g, "
    			+ 	"app_authority a, "
    			+ 	"app_user_group_member gm, "
    			+ 	"app_user_group_auth ga "
    			+ "where u.username = ? "
				+ 	"and u.active=true "
    			+ 	"and gm.user_id = u.id "
    			+	"and gm.group_id = g.id "
    			+	"and ga.group_id = g.id "
				+ 	"and g.active=true "
    			+	"and ga.authority_id = a.id ";
    	
    	auth.jdbcAuthentication()
    		.passwordEncoder(passwordEncoder())
    		.dataSource(dataSource)
    		.usersByUsernameQuery(usersQuery)
    		.authoritiesByUsernameQuery(authoritiesQuery)
    		.groupAuthoritiesByUsername(groupAuthoritiesQuery)
    		.rolePrefix("ROLE_");
    }
    
    @Bean
	public PasswordEncoder passwordEncoder()
	{	
		ShaPasswordEncoder encoder = new ShaPasswordEncoder(512);
		encoder.setEncodeHashAsBase64(true);
		
		return encoder;
	}
}
