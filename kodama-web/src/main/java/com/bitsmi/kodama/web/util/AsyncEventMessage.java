package com.bitsmi.kodama.web.util;

public class AsyncEventMessage 
{
	private String type;
	private String message;
	
	public AsyncEventMessage(String type, String message)
	{
		this.type = type;
		this.message = message;
	}
	
	public String getType()
	{
		return type;
	}
	
	public String getMessage()
	{
		return message;
	}
}
