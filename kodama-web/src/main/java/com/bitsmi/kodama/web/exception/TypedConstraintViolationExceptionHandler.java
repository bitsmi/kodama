package com.bitsmi.kodama.web.exception;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.bitsmi.kodama.core.exception.TypedConstraintViolation;
import com.bitsmi.kodama.core.exception.TypedConstraintViolationException;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class TypedConstraintViolationExceptionHandler 
{
private final Logger log = LoggerFactory.getLogger(TypedConstraintViolationExceptionHandler.class);
	
    @ExceptionHandler({TypedConstraintViolationException.class})
    public void handleConflict(HttpServletRequest request, HttpServletResponse response, TypedConstraintViolationException e) throws Exception 
    {
    	log.error(e.getMessage(), e);
    	
    	response.setStatus(HttpStatus.BAD_REQUEST.value());
    	for(TypedConstraintViolation violation:e.getConstraintViolations()){
    		response.getWriter().println("[" + violation.getCode() + "] - " + violation.getMessage());
    	}    	
    }
}
