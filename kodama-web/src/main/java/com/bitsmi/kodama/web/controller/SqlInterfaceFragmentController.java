package com.bitsmi.kodama.web.controller;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.bitsmi.kodama.core.dto.ISqlResultDto;
import com.bitsmi.kodama.core.dto.SqlMessageDto;
import com.bitsmi.kodama.core.dto.SqlResultsetDto;
import com.bitsmi.kodama.core.exception.BusinessException;
import com.bitsmi.kodama.core.service.ISqlExecutorService;
import com.bitsmi.kodama.web.WebAuthorityConstants;
import com.bitsmi.kodama.web.exception.WebTypedException;

@Controller
@RequestMapping("/fragment/sql")
public class SqlInterfaceFragmentController 
{
	@Autowired
	private ISqlExecutorService sqlExecutorService;
	
	public void setSqlExecutorService(ISqlExecutorService sqlExecutorService)
	{
		this.sqlExecutorService = sqlExecutorService;
	}
	
	@RequestMapping("")
	@PreAuthorize("hasRole('" + WebAuthorityConstants.AUTH_CORE_SQL_VIEW_MAIN + "')")
	public String sessionExpired(Model model) 
	{
		return "sql :: main";
	}
	
	@RequestMapping(value="/execute", method=RequestMethod.POST)
	@PreAuthorize("hasRole('" + WebAuthorityConstants.AUTH_CORE_SQL_VIEW_MAIN + "')")
	@ResponseStatus(value = HttpStatus.OK)
	public String executeStatement(@RequestParam String statement, Model model) throws WebTypedException
	{
		try{
			ISqlResultDto result = sqlExecutorService.executeStatement(statement);
			if(SqlResultsetDto.TYPE.equals(result.getType())){
				List<Map<String, Object>> dataset = ((SqlResultsetDto)result).getResults();
				if(dataset.isEmpty()){
					model.addAttribute("message", "La sentencia no ha produit resultats");
					return "sql :: message";
				}
				
				Set<String> columnNames = dataset.get(0).keySet();
				model.addAttribute("columnNames", columnNames);
				model.addAttribute("dataset", dataset);
				return "sql :: resultset";
			}
			else if(SqlMessageDto.TYPE.equals(result.getType())){
				model.addAttribute("message", ((SqlMessageDto)result).getMessage());
				return "sql :: message";
			}
			
			// No es podrà donar...
			model.addAttribute("message", "La sentencia no ha produit resultats");
			return "sql :: message";
		}
		catch(BusinessException e){
			throw new WebTypedException(e.getErrorCode(), e.getMessage(), e)
					.setFragment(true);
		}
	}
}
