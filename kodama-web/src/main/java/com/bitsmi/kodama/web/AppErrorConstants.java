package com.bitsmi.kodama.web;

public class AppErrorConstants 
{
	/* JOBS */
	/** [APP-8001] S'ha produit un error no especificat durant l'execució del job (&lt;nom job&gt;): &lt;missatge error&gt;  */
	public static final String ERROR_CODE_APP_1001 = "APP-1001";
	public static final String ERROR_DESC_APP_1001 = "S'ha produit un error no especificat durant l'execució del job (%1$s): %2$s";
}
