define(['tether.min'], function(Tether) 
{
    window.Tether = Tether;
    return Tether;
});