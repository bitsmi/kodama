define(['config', 'backbone', 'modules/common/routerModule', 'parsley'],
function(Config, Backbone, RouterModule)
{
	// Definició de la subclasse
	function SqlInterfaceRouter() 
	{
		// Crida al contructor de la superclase.
		RouterModule.AbstractSubrouter.call(this); 
	}
	// Definició de la subclasse
	SqlInterfaceRouter.prototype = Object.create(RouterModule.AbstractSubrouter.prototype);
	SqlInterfaceRouter.prototype.constructor = SqlInterfaceRouter;
	// Extensió de les funcions proporcionades
	_.extend(SqlInterfaceRouter.prototype, RouterModule.AbstractSubrouter.prototype, {
		routes:{
			"":"showMain"
		},
		
		initializeView: function(){
	    	if(RouterModule.App.currentView===undefined){
	    		RouterModule.App.currentView = new SqlInterfaceView({el: "#main-content"});
	    	}
	    	else if(!(RouterModule.App.currentView instanceof SqlInterfaceView)){
	    		RouterModule.App.unloadCurrentView();
	    		RouterModule.App.currentView = new SqlInterfaceView({el: "#main-content"});
			}
	    },
	    
	    showMain: function(pathVariables, params){
	        this.initializeView();
	        RouterModule.App.currentView.load();
	    }
	});			
	
	/**
	 * @param title: undefined,
	 * @param url: undefined,
	 * @param bodyTemplate: undefined,
	 * @param doneActionTitle: Acceptar
	 * @param cancelActionTitle: Cancel·lar
	 * @param callbackHandler: undefined,
	 * @param extraData: undefined
	 */
	var SqlInterfaceView = Backbone.View.extend({
    	/* Paràmetres exposats al constructor */
    	defaults: {
			url: Config.contextPath + "/fragment/sql",
        },
        
        /* Funcions */
        initialize:function(options){
        	// Merge dels valors passats per paràmetre amb els default
        	this.options = options;
            _.defaults(this.options, this.defaults);
        },
        remove: function() {
            this.$el.empty().off(); /* Unbind dels events */
            this.stopListening();
            return this;
        },
        events: {
            "click #submitForm": "executeSql"
        },
        
        /* Funcions custom */
        load: function(){
    		var that = this;
        	this.$el.load(this.options.url, function(){
        		that.onPageLoaded();
        	});
        },
        /**
         * Callback executat per la funció showPopup en el moment en que es carrega la pàgina. 
         * S'haurà de sobreescriure si s'exten la vista.
         */
        onPageLoaded: function()
        {
        	
        },
        unload: function()
        {
        	this.remove();
        },
        
        executeSql: function(e)
        {
        	e.stopPropagation();
        	
        	var that = this;
        	var success = false;
			this.$el.find('#sqlForm').parsley()
				.on('form:validate', function(){
					that.$el.find('.form-group').removeClass("has-danger");
				})
				.on('form:success', function(){
					success = true;
				})
				.on('form:error', function(){
					
				})
				.on('field:error', function(){
					this.$element.parent(".form-group").addClass("has-danger");
				})
				.validate();
			
			if(!success){
				return;
			}
			
			// Realització de la tasca
			var params = {
					statement: this.$el.find("textarea[name='scriptTextarea']").val()
				}
			var csrfName = this.$el.find(".csrfField").attr("name");
			params[csrfName] = this.$el.find(".csrfField").val();
			$.ajax({
				url: "/fragment/sql/execute",
				method: "POST",
				data: params
			})
			.done(function(data)
			{
				that.$el.find("#sqlResult").html(data);
			})			
        }
    });
	
    return{
    	SqlInterfaceRouter: SqlInterfaceRouter,
    	SqlInterfaceView: SqlInterfaceView
    }
});