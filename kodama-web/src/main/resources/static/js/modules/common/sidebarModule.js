define(['config', 
        'backbone',
        "modules/sqlInterfaceModule",
        'dcjqaccordion',
        'nicescroll',
        'scrollTo'],
function(Config, Backbone, SqlInterfaceModule)
{
	var SidebarView = Backbone.View.extend({
    	/* Paràmetres exposats al constructor */
    	defaults: {
        	
        },
        
        /* Funcions */
        initialize:function(options){
        	// Merge dels valors passats per paràmetre amb els default
        	this.options = options;
            _.defaults(this.options, this.defaults);

            this.$el.find('#nav-accordion').dcAccordion({
    	        eventType: 'click',
    	        autoClose: true,
    	        saveState: true,
    	        // Deshabilitar la icona de la fletxa per desplegar
    	        classArrow: '',
    	        // Disable all links of parent items
    	        disableLink: true,	
    	        speed: 'normal',
    	        showCount: false,
    	        autoExpand: false,
//    	        cookie: 'dcjq-accordion-1',
    	        classExpand: 'dcjq-current-parent'
    	    });
    		// Scrollbar
            this.$el.niceScroll({
            	styler: "fb",
            	cursorcolor: "#4ECDC4", 
            	cursorwidth: '3', 
            	cursorborderradius: '10px', 
            	background: '#404040', 
            	spacebarenabled: false, 
            	cursorborder: ''
            });
        },
        remove: function() {
            this.$el.empty().off(); /* Unbind dels events */
            this.stopListening();
            return this;
        },
        events: {
            "click .sub-menu > a": "onMenuClick"
        },
         /* Funcions custom */
        // sidebar dropdown menu auto scrolling
        onMenuClick: function(event)
        {
	        var offset = ($(event.target).offset());
	        diff = 250 - offset.top;
	        if(diff>0){
	            this.$el.scrollTo("-="+Math.abs(diff),500);
	        }
	        else{
	        	this.$el.scrollTo("+="+Math.abs(diff),500);
	        }
        }
    });
	
    return{
        SidebarView: SidebarView
    }
});