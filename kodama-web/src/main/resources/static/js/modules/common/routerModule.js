define(['config', 
        'backbone',
        'URI'],
function(Config, Backbone, URI)
{
	var App = {
		// referència a la vista actualment carregada a la pantalla
		currentView: undefined,
		applicationRouter: undefined,
		
		unloadCurrentView: function(){
			if(this.currentView===undefined){
				return;
			}
				
			if(this.currentView.unload!==undefined){
				this.currentView.unload();
        	}
		}
	};
	
	var ApplicationAbstractRouter = Backbone.Router.extend(
	{
		defaults: {
			subrouters: undefined,
		},
		
		initialize:function(options){
        	// Merge dels valors passats per paràmetre amb els default
			if(options==undefined){
				this.options = {};
			}
			else{
				this.options = options;
			}
            _.defaults(this.options, this.defaults);
            
            // Definició de les rutes
            var customRoutes = [
                [/^$/, "invokeRootModule", this.invokeRootModule],
                [/^([\w-_]+)$/, "invokeModule", this.invokeModule],
                [/^([\w-_]+)\/([\w-_\/=?&]+)$/, "invokeModuleSubroute", this.invokeModule],
                [/^([\w-_]+)\?([\w-_\/=&]+)$/, "invokeModuleWithParams", this.invokeModuleWithParams]
            ]
            
            var that = this;
            _.each(customRoutes, function(route){
                that.route.apply(that, route);
            });
            
            Backbone.history.start();
		},
		
		routes: {
			// Les rutes es definiran a través d'expressions regulars durant la inicialització
		},
	
		invokeRootModule: function(){
			// Res a fer per ara...
		},
		
		invokeModule: function(module, subroute){
			this.invokeModuleDelegate(module, subroute, true);
		},
		invokeModuleWithParams: function(module, subroute){
			this.invokeModuleDelegate(module, subroute, false);
		},
		invokeModuleDelegate: function(module, subroute, isUrl){
			var moduleRouterId = module.toLowerCase();
			var subrouter = this.options.subrouters[moduleRouterId]; 
	        if(!subrouter) {
	        	throw "Router módul no reconegut: " + module;
	        }
	        
	        if(subroute!=null){
	        	if(isUrl){
		        	subroute = "/" + subroute;
		        }
		        else{
		        	subroute = "?" + subroute;
		        }
	        }
	        
	        var params = {};
	        if(subroute!=null){
	        	params = new URI(subroute).query(true);
	        }
	        
	        subrouter.delegate(subroute, params);
		}
	});
	
	/* Definició d'un Subrouter */
	// Definició de la subclasse
	function AbstractSubrouter() 
	{
		 
	}
	// Definició de la subclasse
	AbstractSubrouter.prototype = Object.create(Object.prototype);
	AbstractSubrouter.prototype.constructor = AbstractSubrouter;
	// Extensió de les funcions proporcionades
	_.extend(AbstractSubrouter.prototype, Object.prototype, {
		delegate: function(subroute, params){
    		
			if(subroute==null){
				subroute = "";
			}
			
			var router = this;
    		_.every(this.routes, function(callback, route) {
                 // Use the Backbone parser to turn route into regex for matching
    			 if (!_.isRegExp(route)){
    				 route = Backbone.Router.prototype._routeToRegExp(route)
    			 }
                 if(subroute.match(route)){
                	 
                	 var pathVariables = Backbone.Router.prototype._extractParameters(route, subroute);
                	 // Si l'últim valor == null, correspon a la query string. S'exclou
                	 if(_.last(pathVariables)===null){
                		 pathVariables = _.initial(pathVariables);
                	 }
                	 
                	 var callbackFunction = this[callback];
                     callbackFunction.call(this, pathVariables, params)
                     return false;
                 }
                 
                 return true;
            }, this);
		}
	});	
		
    return{
    	App: App,
        ApplicationAbstractRouter: ApplicationAbstractRouter,
        AbstractSubrouter: AbstractSubrouter
    }
});