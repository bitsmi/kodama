define(['config', 'backbone', 'modules/common/popupModule', 'handlebars','moment'],
function(Config, Backbone, PopupModule, Handlebars, Moment)
{
	var COMMAND_REGEX = /\:([\w\.]+){0,1}(?:\W(.+)){0,1}/

	// Definició del bus d'events entre vistes
	Backbone.View.prototype.eventAggregator = _.extend({}, Backbone.Events);
		
	var MultisearchView = Backbone.View.extend({
		
		defaults:{
			name: undefined,
			commands: new Array()
		},
		
		multisearchOptions: undefined,
		multisearchCommandTemplate: undefined,
		multisearchFilterTemplate: undefined,

		commandCache: new Object(),
		
    	/* Funcions */
        initialize:function(options){
        	// Merge dels valors passats per paràmetre amb els default
        	this.options = options;
            _.defaults(this.options, this.defaults);
            
            // S'indexa l'accés a les dades de les comandes a partir de les dades especificades
            _.each(this.options.commands, function(element, index, list){
        		this.commandCache[element.command] = element; 
        	}, this);
            
            this.multisearchOptions = this.$el.find("#multisearch_" + this.options.name + "_options");
            this.multisearchCommandTemplate = Handlebars.compile($("#templateMultisearchCommand").html());
            this.multisearchFilterTemplate = Handlebars.compile($("#templateMultisearchFilter").html());
            
            var that = this;
            this.$el.find('div.datepickerPlaceholder')
				.on("dp.change", function(event){
	        		var inputCommand = that.$el.find("input.placeholder").val();
	    			var match = COMMAND_REGEX.exec(inputCommand);
	    			if(match!=null){
	        			var command = match[1];
	        			if(command!=undefined){
		        			that.$el.find("input.placeholder").val(":" + command + " " + Moment(event.date).format('DD/MM/YYYY'));
		    			}
	    			}
	    			that.$el.find('div.datepickerPlaceholder').hide();
				})
				.datetimepicker({
					format: 'DD/MM/YYYY',
					inline: true
				});
            
            this.render();
        },
        render:function(){      
        	_.each(this.options.commands, function(element, index, list){
        		this.multisearchOptions.append(this.multisearchCommandTemplate(element));
        	}, this);
        	// Es fa així perquè no funciona el selector a la secció d'events
        	this.multisearchOptions.find("li a").on("click", $.proxy(this.commandSelected, this));
        },
        remove: function() {
            this.$el.empty().off(); /* Unbind dels events */
            this.stopListening();
            return this;
        },
        
        /* Declaració d'events */
        events: function(){
        	vEvents = {};
        	vEvents["keyup input.placeholder"] = "onInputKeyPress";
            
            return vEvents;
        },
        
        /* Funcions custom */
        onInputKeyPress: function(event){
        	// Escape
        	if(event.which==27){
        		if(this.multisearchOptions.is(":visible")){
        			this.multisearchOptions.dropdown().toggle();
        		}
        		if(this.$el.find('div.datepickerPlaceholder').is(":visible")){
        			this.$el.find('div.datepickerPlaceholder').hide();
        		}
    		}
        	// Enter
        	else if(event.which==13){
        		var inputCommand = this.$el.find("input.placeholder").val();
    			var match = COMMAND_REGEX.exec(inputCommand);
    			if(match==null){
    				// TODO Default
    			}
    			else{
        			var command = match[1];
        			var value = match[2];
        			
        			var objCommand = this.commandCache[command];
        			if(objCommand==null){
        				PopupModule.Helpers.showWarningMessage("#popupPlaceholder", "El filtre introduït no és vàlid");
        				return;
        			}

        			if(objCommand.type=="TEXT"
        					&& (value==null || value.length==0)){
        				PopupModule.Helpers.showWarningMessage("#popupPlaceholder", "El valor del filtre de text no pot ser buit");
        				return;
        			}
        			if(objCommand.type=="DATE"
        					&& !Moment(value, "DD/MM/YYYY").isValid()){
        				PopupModule.Helpers.showWarningMessage("#popupPlaceholder", "El valor del filtre de data no és vàlid [DD/MM/YYYY]");
        				return;
        			}
        			else if(objCommand.type=="BOOLEAN"
        					&& (value==null 
        							|| value.toUpperCase()!=="TRUE" 
        							&& value.toUpperCase()!=="FALSE")){
        				PopupModule.Helpers.showWarningMessage("#popupPlaceholder", "El valor del filtre booleà no és vàlid [true|false]");
        				return;
        			}
        			
        			var templateParams = {
        				type: objCommand.type,
        				command: command,
        				value: value
        			}
        			$('#multisearch_' + this.options.name + '_active_filters').append(this.multisearchFilterTemplate(templateParams));
        			this.$el.find("input.placeholder").val("");
        			
        			this.eventAggregator.trigger("event_multisearch_" + this.options.name);
    			}
        	}
        	// Espai
        	else if(event.which==0 || event.which==32){
        		if(this.multisearchOptions.is(":visible")){
    				this.multisearchOptions.dropdown().toggle();
    			}
        		
        		var inputCommand = this.$el.find("input.placeholder").val();
    			var match = COMMAND_REGEX.exec(inputCommand);
    			if(match==null){
    				return;
    			}
    			else{
        			var command = match[1];
        			var value = match[2];
        			
        			var objCommand = this.commandCache[command];
        			if(objCommand==null){
        				PopupModule.Helpers.showWarningMessage("#popupPlaceholder", "El filtre introduït no és vàlid");
        				return;
        			}
        			
        			// TEXT: Res a fer...
        			// BOOLEAN: Res a fer...
        			// DATE: Mostrar datepicker
        			if(objCommand.type=="DATE"){
        				if(this.multisearchOptions.is(":visible")){
        	    			this.multisearchOptions.dropdown().toggle();
        	    		}
        				
        				this.$el.find('div.datepickerPlaceholder').show();
        			}
    			}
    		}
/*        	
        	// Up
        	else if(event.which==38){
        		var selectedCommand = this.multisearchOptions.find("li.commandDescriptor.selected:visible");
        		if(selectedCommand.length==0){
        			this.multisearchOptions.find("li.commandDescriptor:visible").last().addClass("selected");
        		}
        		else{        		
        			selectedCommand.removeClass("selected");
        			selectedCommand.prev(".commandDescriptor:visible").addClass("selected");
        		}
        	}
        	// Down
        	else if(event.which==40){
        		var selectedCommand = this.multisearchOptions.find("li.commandDescriptor.selected:visible");
        		if(selectedCommand.length==0){
        			this.multisearchOptions.find("li.commandDescriptor:visible").first().addClass("selected");
        		}
        		else{
        			selectedCommand.removeClass("selected");
        			selectedCommand.next(".commandDescriptor:visible").addClass("selected");
        		}
        	}
 */        	
        	else{
        		this.displayOptions(event);
        	}
        },
        
        displayOptions: _.debounce(function(event){
    		var inputCommand = this.$el.find("input.placeholder").val();
    		var match = /\:\w+\W+.*/.exec(inputCommand);
			if(match!=null){
				return;
			}
    		
    		if(inputCommand.substring(0,1) == ":"){
    			if(!this.multisearchOptions.is(":visible")
    					&& !this.$el.find('div.datepickerPlaceholder').is(":visible")){
    				this.multisearchOptions.dropdown().toggle();
    			}
    			
    			var match = COMMAND_REGEX.exec(inputCommand);
    			if(match!=null){
        			var command = match[1];
        			var value = match[2];
        			
        			if(command==null){
        				this.multisearchOptions.find("li").show();
        			}
        			else{
        				this.multisearchOptions.find("li:has(a[data-command^='" + command + "'])").show();
        				this.multisearchOptions.find("li:has(a:not([data-command^='" + command + "']))").hide();
        			}
    			}
        	}
        }, 1000),
        
        commandSelected: function(event){
        	var dataCommand = $(event.target).closest("a[data-command]").attr("data-command");
    		this.$el.find("input.placeholder").val(":" + dataCommand);
        }
    });
	
	var MultisearchActiveFiltersView = Backbone.View.extend({
		
		defaults:{
			name: undefined,
			url: undefined
		},		
		
    	/* Funcions */
        initialize:function(options){
        	// Merge dels valors passats per paràmetre amb els default
        	this.options = options;
            _.defaults(this.options, this.defaults);
            
            // Delegar events d'esborrar
            this.$el.delegate("div.multisearchFilter button", "click", $.proxy(function(event){
            	$(event.target).closest("div").remove();
            	
            	this.doFilter();
            }, this));
            
            /* Bindings a events de la vista */
	    	// El tercer paràmetre indica el context que ha d'executar la funció. D'aquesta manera les variables de la vista estaran disponibles
	    	this.eventAggregator.bind("event_multisearch_" + this.options.name, this.doFilter, this);
        },
        remove: function() {
            this.$el.empty().off(); /* Unbind dels events */
            this.stopListening();
            return this;
        },
        
        /* Declaració d'events */
        events: function(){
        	vEvents = {};
            
            return vEvents;
        },
        
        /* Funcions custom */
        doFilter: function(){
        	window.location = this.options.url + "?filter=" + this.getActiveFilters();
        },
        
        getActiveFilters: function(){
        	var filters = new Object();
        	var filterObjs = this.$el.find(".multisearchFilter");
        	_.each(filterObjs, function(elem, index, list){
        		var currentFilter = {
        				type: $(elem).attr("data-type"),
        				command: $(elem).attr("data-command"),
        				value: $(elem).attr("data-value")
        		}
        		
        		var commandList = filters[currentFilter.command];
        		if(commandList==null){
        			filters[currentFilter.command] = [currentFilter];
        		}
        		else{
        			commandList.push(currentFilter);
        		}
        	}, this);
        	
        	var encodedFilters = btoa(JSON.stringify(filters));
    		return encodedFilters;
        }
    });
	
    // Constructor: Assigna totes les propietats passades al contructor
    var MultisearchCommand = function(type, typeDesc, command, description, url)
	{
    	var options = {
    		type: type,
    		typeDesc: typeDesc,
    		command: command,
    		description: description,
    		url: url
    	}
    	
    	_.extend(this, options);
	};
	
	var Helpers = 
    {
		createTextCommand: function(command, description)
		{
			return new MultisearchCommand("TEXT", "Text lliure", command, description);
		},
		createBooleanCommand: function(command, description)
		{
			return new MultisearchCommand("BOOLEAN", "Booleà", command, description);
		},
		createDateCommand: function(command, description)
		{
			return new MultisearchCommand("DATE", "Data", command, description);
		}
    }
     
    return{
        MultisearchView: MultisearchView,
        MultisearchActiveFiltersView: MultisearchActiveFiltersView,
        MultisearchCommand: MultisearchCommand,
        Helpers: Helpers
    }
});