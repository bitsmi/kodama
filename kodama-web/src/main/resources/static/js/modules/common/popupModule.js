define(['config', 'backbone', 'handlebars'],
function(Config, Backbone, Handlebars)
{
	/**
	 * @param title: undefined,
	 * @param url: undefined,
	 * @param templateSelector: '#templatePopup'
	 * @param bodyTemplate: undefined,
	 * @param doneActionTitle: Acceptar
	 * @param cancelActionTitle: Cancel·lar
	 * @param callbackHandler: undefined,
	 * @param extraData: undefined
	 */
	var PopupView = Backbone.View.extend({
    	/* Paràmetres exposats al constructor */
    	defaults: {
        	title: undefined,
			url: undefined,
			templateSelector: '#templatePopup',
			bodyTemplate: undefined,
			doneActionTitle: "Acceptar",
			cancelActionTitle: "Cancel·lar",
			callbackHandler: undefined,
			extraData: undefined
        },
        
        /* Atributs privats de la vista */
        objBodyTemplate: undefined,
        
        /* Funcions */
        initialize:function(options){
        	// Merge dels valors passats per paràmetre amb els default
        	this.options = options;
            _.defaults(this.options, this.defaults);
        	
            this.template = Handlebars.compile($(this.options.templateSelector).html());
            if(this.options.bodyTemplate!==undefined){
            	// Cas que el bodyTemplate sigui un identificador
            	if($(this.options.bodyTemplate)!==undefined){
            		this.objBodyTemplate = Handlebars.compile($(this.options.bodyTemplate).html());
            	}
            	// Cas que sigui directament el codi del template
            	else{
            		this.objBodyTemplate = Handlebars.compile(this.options.bodyTemplate);
            	}
            }
        },
        render:function(){        	
            this.$el.html(this.template(this.options));
        },
        remove: function() {
            this.$el.empty().off(); /* Unbind dels events */
            this.stopListening();
            return this;
        },
        events: {
            "click #doneAction": "doneAction",
            "click #cancelAction": "cancelAction"
        },
        
        /* Funcions custom */
        showPopup: function(){
        	if($.fn.modal!==undefined){
	        	$.fn.modal.Constructor.prototype.enforceFocus = function() {
	    	        // Hack per solucionar bug amb Chrome: Uncaught RangeError: Maximum
	    			// call stack size exceded
	    		};
        	}
        	
        	this.render();
        	
        	if(this.options.url!==undefined){
        		var that = this;
            	this.$el.find(".modal-body").load(this.options.url, function(){
            		that.onPageLoaded();
            	});
        	}
        	else if(this.objBodyTemplate!==undefined){
        		this.$el.find(".modal-body").html(this.objBodyTemplate(this.options));
        		this.onPageLoaded();
        	}
        	
        	var that = this;
        	this.$el.find(".modal").on('show', function()
        	{
        		if(!$(this).hasClass('modalVisible')){
	        		var zIndex = Math.max.apply(null, [].map.call(document.querySelectorAll('*'), function(el) {
	        			  return el.style.zIndex;
	    			})) + 10;        		
	        	    $(this).css('z-index', zIndex);
	        	    $(this).addClass('modalVisible');
	        	    setTimeout(function() {
	        	        $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
	        	    }, 0);
        		}
        	}).on('hidden.bs.modal', function(e)
        	{
        		if($(e.target).hasClass("modal")){
        			that.remove();
        		}
        	}).modal({backdrop: 'static', 
        		keyboard: false,
        		show: true, 
        		position: 'fixed'});
        	
        	var viewportHeight = $(window).height();
        	var maxHeight = Math.min(viewportHeight - 200, 500);
        	this.$el.find(".modal-body").css("max-height", maxHeight);
        },
        /**
         * Callback executat per la funció showPopup en el moment en que es carrega la pàgina. 
         * S'haurà de sobreescriure si s'exten la vista.
         */
        onPageLoaded: function()
        {
        	
        },
        doneAction: function()
        {
        	if(this.options.callbackHandler!==undefined){
        		if(!this.options.callbackHandler.onDone()){
        			return false;
        		}
        	}
        	this.close();
        },
        cancelAction: function()
        {
        	if(this.options.callbackHandler!==undefined){
        		this.options.callbackHandler.onCancel();
        	}
        	this.close();
        },
        close: function()
        {
        	this.$el.find(".modal").modal('hide');
        }
    });
	
	var ErrorPopupView = Backbone.View.extend({
    	/* Paràmetres exposats al constructor */
    	defaults: {
    		title: undefined,
    		errorCode: undefined,
    		errorMessage: undefined
        },
        
        /* Atributs privats de la vista */
        
        /* Funcions */
        initialize:function(options){
        	// Merge dels valors passats per paràmetre amb els default
        	this.options = options;
            _.defaults(this.options, this.defaults);
        	
            this.template = Handlebars.compile($('#templateErrorPopup').html());            
        },
        render:function(){        	
            this.$el.html(this.template(this.options));
        },
        remove: function() {
            this.$el.empty().off(); /* Unbind dels events */
            this.stopListening();
            return this;
        },
        events: {
            "click #closeAction": "closeAction"
        },
        
        /* Funcions custom */
        showPopup: function(){
        	this.render();
        	
        	var that = this;
        	this.$el.find(".modal").on('hidden.bs.modal', function(e)
        	{
        		if($(e.target).hasClass("modal")){
        			that.remove();
        		}
        	}).modal({backdrop: 'static', 
        		keyboard: false,
        		show: true, 
        		position: 'fixed'
        	});        	
        },
        closeAction: function()
        {
        	this.$el.find(".modal").modal('hide');
        }
    });
	
	var ProgressPopupView = Backbone.View.extend({
		/* Paràmetres exposats al constructor */
    	defaults: {
			templateSelector: '#templateProgressPopup'
        },
		
    	/* Funcions */
        initialize:function(options){
        	// Merge dels valors passats per paràmetre amb els default
        	this.options = options;
            _.defaults(this.options, this.defaults);
        	
            this.template = Handlebars.compile($(this.options.templateSelector).html());            
        },
        render:function(){        	
            this.$el.html(this.template(this.options));
        },
        remove: function() {
            this.$el.empty().off(); /* Unbind dels events */
            this.stopListening();
            return this;
        },
        events: {
        	"click #dismissAction": "close"
        },
        /* Funcions custom */
        showPopup: function(){
        	this.render();
        	
        	var that = this;
        	this.$el.find(".modal").on('hidden.bs.modal', function(e)
        	{
        		if($(e.target).hasClass("modal")){
        			that.remove();
        		}
        	}).modal({backdrop: 'static', 
        		keyboard: false,
        		show: true, 
        		position: 'fixed'
        	});        	
        },
        progress: function(progressAmount, message){
        	var txtMessage = progressAmount + "%";
        	if(message!==undefined && message!=null){
        		txtMessage = message + " " + txtMessage; 
        	}
        	this.$el.find("#progressCaption").text(txtMessage);
        	this.$el.find("progress").val(progressAmount);
        },
        error: function(errorMessage){
        	this.$el.find("#progressCaption").text("Error: " + errorMessage);
        	this.$el.find("progress").addClass("danger");
        	
        	this.$el.find("#dismissAction").parent().show();
        },
        close: function()
        {
        	this.$el.find(".modal").modal('hide');
        }
    });
	
	/* Definició d'un handler predeterminat per les vistes popup */
    // Constructor: Assigna totes les propietats passades al contructor
    var PopupViewCallbackHandler = function(options)
	{
    	_.extend(this, options);
	};
	// Funcions per defecte
	_.extend(PopupViewCallbackHandler.prototype, {
    	onValidate: function()
    	{
    		
    	},
		onDone: function()
    	{
    		
    	},
    	onCancel: function()
    	{
    		
    	}
	});
	
	var Helpers = 
    {
		showWarningMessage: function(placeholder, errorMessage)
		{
			var warningView = new ErrorPopupView({el: placeholder, 
	        	title: "Advertència",
	        	errorCode: undefined,
	        	errorMessage: errorMessage
	        });
			warningView.showPopup();
		},
    		
		showErrorMessage: function(placeholder, errorCode, errorMessage)
		{
			var errorView = new ErrorPopupView({el: placeholder, 
	        	title: "Error",
	        	errorCode: errorCode,
	        	errorMessage: errorMessage
	        });
	    	errorView.showPopup();
		},
		
		createPopupViewCallbackHandler: function(options, extendedFunctions)
		{
			// Definició de la subclasse
			function Handler() 
			{
				// Crida al contructor de la superclase.
				PopupViewCallbackHandler.call(this, options); 
			}

			// Definició de la subclasse
			Handler.prototype = Object.create(PopupViewCallbackHandler.prototype);
			Handler.prototype.constructor = Handler;
			// Extensió de les funcions proporcionades
			_.extend(Handler.prototype, PopupViewCallbackHandler.prototype, extendedFunctions);
			
			return new Handler(options);
		}
    }
     
    return{
        PopupView: PopupView,
        ProgressPopupView: ProgressPopupView,
        Helpers: Helpers
    }
});