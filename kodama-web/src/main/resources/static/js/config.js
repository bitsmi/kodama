requirejs.config({
    // Per defecte es carrega qualsevol mòdul de js/lib
    baseUrl: '/js/lib',
    /* Els paths són relatius a baseUrl i mai incluen ".js" com extensió
     */
    paths: {
    	"app": "../app",
    	"modules": "../modules",
    	
    	"jquery": "jquery.min",
    	"backbone": "backbone-min",
    	"underscore": "underscore-min",
    	"underscore.string": "underscore.string",
    	"handlebars": "handlebars-v3.0.3",
    	"datepicker": "bootstrap-datetimepicker.min",
    	"moment": "moment.min",
    	"dcjqaccordion": "jquery.dcjqaccordion.2.7",
    	"nicescroll": "jquery.nicescroll",
    	"scrollTo": "jquery.scrollTo.min",
    	"tether": "tether-wrapper"
    },
	shim: {
		"bootstrap": {
		    deps: ["jquery", "tether"],
		    exports: 'bootstrap'
		},
		"datepicker": {
			deps: ['moment', 'jquery', 'bootstrap'],
			init: function(moment){
                window.moment = moment;
            }
		},
		"dcjqaccordion": {
			deps: ['jquery'],
	        exports: 'jQuery.fn.dcAccordion'
		},
		"nicescroll": {
			deps: ['jquery'],
	        exports: 'jQuery.fn.nicescroll'
		},
		"scrollTo": {
			deps: ['jquery'],
	        exports: 'jQuery.fn.scrollTo'
		}
	}
});
