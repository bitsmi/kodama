requirejs(['config', 
           'backbone',
           'modules/common/sidebarModule',
           'modules/common/routerModule',
           'modules/sqlInterfaceModule',
           'dcjqaccordion',
           'nicescroll',
           'scrollTo',
           'datepicker',
           'parsley'],
function(Config, Backbone, SidebarModule, RouterModule, SqlInterfaceModule)
{
	// Scroll pantalla
	$("html").niceScroll({styler: "fb",
		cursorcolor: "#4ECDC4", 
		cursorwidth: '6', 
		cursorborderradius: '10px', 
		background: '#404040', 
		spacebarenabled: false,  
		cursorborder: '', 
		zindex: '1000'
	});
	
	var sidebarView = new SidebarModule.SidebarView({el:"#sidebar"});
	
	var ApplicationRouter = RouterModule.ApplicationAbstractRouter.extend({
		defaults: _.extend({}, RouterModule.ApplicationAbstractRouter.prototype.defaults, {
			subrouters:{
				"sql": new SqlInterfaceModule.SqlInterfaceRouter()
			}
		})
	});
	
	new ApplicationRouter();
	
	// TODO sidebar toggle (S'ha d'integrar en els móduls de les vistes)
    $(function() {
        function responsiveView() {
            var wSize = $(window).width();
            if (wSize <= 768) {
                $('#topMenu').addClass('sidebar-close');
                $('#sidebar > ul').hide();
            }

            if (wSize > 768) {
                $('#topMenu').removeClass('sidebar-close');
                $('#sidebar > ul').show();
            }
        }
        $(window).on('load', responsiveView);
        $(window).on('resize', responsiveView);
    });

    $('.fa-bars').click(function () {
        if ($('#sidebar > ul').is(":visible") === true) {
            $('#main-content').css({
                'margin-left': '0px'
            });
            $('#sidebar').css({
                'margin-left': '-210px'
            });
            $('#sidebar > ul').hide();
            $("#container").addClass("sidebar-closed");
        } else {
            $('#main-content').css({
                'margin-left': '210px'
            });
            $('#sidebar > ul').show();
            $('#sidebar').css({
                'margin-left': '0'
            });
            $("#container").removeClass("sidebar-closed");
        }
    });

});

