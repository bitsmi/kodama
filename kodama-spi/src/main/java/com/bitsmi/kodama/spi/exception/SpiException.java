package com.bitsmi.kodama.spi.exception;

public class SpiException extends TypedException
{
	private static final long serialVersionUID = -8543041899695242262L;

	public SpiException(String errorCode) 
	{
		super(errorCode);
	}

	public SpiException(String errorCode, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) 
	{
		super(errorCode, message, cause, enableSuppression, writableStackTrace);
	}

	public SpiException(String errorCode, String message, Throwable cause) 
	{
		super(errorCode, message, cause);
	}

	public SpiException(String errorCode, String message) 
	{
		super(errorCode, message);
	}

	public SpiException(String errorCode, Throwable cause) 
	{
		super(errorCode, cause);
	}
}
