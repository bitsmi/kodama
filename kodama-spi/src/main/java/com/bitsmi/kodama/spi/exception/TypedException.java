package com.bitsmi.kodama.spi.exception;

public class TypedException extends Exception
{
	private static final long serialVersionUID = -2554570015427921107L;
	
	private String errorCode;
	
	public TypedException(String errorCode) 
	{
		super();
		this.errorCode = errorCode;
	}

	public TypedException(String errorCode, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) 
	{
		super(message, cause, enableSuppression, writableStackTrace);
		this.errorCode = errorCode;
	}

	public TypedException(String errorCode, String message, Throwable cause) 
	{
		super(message, cause);
		this.errorCode = errorCode;
	}

	public TypedException(String errorCode, String message) 
	{
		super(message);
		this.errorCode = errorCode;
	}

	public TypedException(String errorCode, Throwable cause) 
	{
		super(cause);
		this.errorCode = errorCode;
	}

	public String getErrorCode() 
	{
		return errorCode;
	}

	public void setErrorCode(String errorCode) 
	{
		this.errorCode = errorCode;
	}
}
