package com.bitsmi.kodama.spi.exception;

public class TypedRuntimeException extends RuntimeException
{
	private static final long serialVersionUID = -6458972163685438540L;
	
	private String errorCode;
	
	public TypedRuntimeException(String errorCode) 
	{
		super();
		this.errorCode = errorCode;
	}

	public TypedRuntimeException(String errorCode, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) 
	{
		super(message, cause, enableSuppression, writableStackTrace);
		this.errorCode = errorCode;
	}

	public TypedRuntimeException(String errorCode, String message, Throwable cause) 
	{
		super(message, cause);
		this.errorCode = errorCode;
	}

	public TypedRuntimeException(String errorCode, String message) 
	{
		super(message);
		this.errorCode = errorCode;
	}

	public TypedRuntimeException(String errorCode, Throwable cause) 
	{
		super(cause);
		this.errorCode = errorCode;
	}

	public String getErrorCode() 
	{
		return errorCode;
	}

	public void setErrorCode(String errorCode) 
	{
		this.errorCode = errorCode;
	}
}
