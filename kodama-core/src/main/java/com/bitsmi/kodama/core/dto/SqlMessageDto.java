package com.bitsmi.kodama.core.dto;

public class SqlMessageDto implements ISqlResultDto 
{
	public static final String TYPE = "SQL_MESSAGE";
	
	private String message;
	
	public SqlMessageDto(String message)
	{
		this.message = message;
	}
	
	@Override
	public String getType()
	{
		return TYPE;
	}
	
	public String getMessage()
	{
		return message;
	}
}