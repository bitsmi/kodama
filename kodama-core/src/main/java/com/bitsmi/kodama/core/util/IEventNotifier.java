package com.bitsmi.kodama.core.util;

import java.io.IOException;

public interface IEventNotifier<T> 
{
	public void send(T object) throws IOException;
	public void notifyError(String errorCode, String errorMessage) throws IOException;
	public void complete() throws IOException;
}
