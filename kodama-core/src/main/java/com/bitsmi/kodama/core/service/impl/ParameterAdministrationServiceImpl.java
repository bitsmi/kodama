package com.bitsmi.kodama.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.bitsmi.kodama.core.domain.Parameter;
import com.bitsmi.kodama.core.domain.factory.ParameterFactory;
import com.bitsmi.kodama.core.repository.IParameterRepository;
import com.bitsmi.kodama.core.service.IParameterAdministrationService;

@Service
public class ParameterAdministrationServiceImpl implements IParameterAdministrationService 
{
	@Autowired
	private IParameterRepository parameterRepository;
	
	public void setParameterRepository(IParameterRepository parameterRepository)
	{
		this.parameterRepository = parameterRepository;
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public Parameter findParameterByKey(String key)
	{
		return parameterRepository.findByParamKey(key);
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public String findParameterValueByKey(String key)
	{
		return findParameterValueByKey(key, null);
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public String findParameterValueByKey(String key, String defaultValue)
	{
		Parameter parameter = findParameterByKey(key);
		if(parameter!=null){
			return parameter.getParamValue();
		}
		
		return defaultValue;
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void saveParameter(String key, String value)
	{
		Parameter parameter = parameterRepository.findByParamKey(key);
		if(parameter==null){
			parameter = ParameterFactory.newParameter(key, value);
		}
		else{
			parameter.setParamValue(value);
		}
		parameterRepository.save(parameter);
	}
	
	@Transactional(rollbackFor=Exception.class)
	@Override
	public void deleteParameter(String key)
	{
		Parameter parameter = parameterRepository.findByParamKey(key);
		if(parameter!=null){
			parameterRepository.delete(parameter);
		}
	}
}
