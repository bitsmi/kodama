package com.bitsmi.kodama.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bitsmi.kodama.core.domain.Parameter;

public interface IParameterRepository extends JpaRepository<Parameter, Long>
{
	public Parameter findById(Long id);
	public Parameter findByParamKey(String key);
}
