package com.bitsmi.kodama.core.service;

import com.bitsmi.kodama.core.dto.ISqlResultDto;
import com.bitsmi.kodama.core.exception.BusinessException;

public interface ISqlExecutorService 
{
	public ISqlResultDto executeStatement(String statement) throws BusinessException;
}
