package com.bitsmi.kodama.core.domain.factory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import com.bitsmi.kodama.core.CoreErrorConstants;
import com.bitsmi.kodama.core.domain.User;
import com.bitsmi.kodama.core.exception.TypedConstraintViolation;
import com.bitsmi.kodama.core.exception.TypedConstraintViolationException;

public class UserFactory 
{
	public static User newUser(String username,
			String password)
	{
		return builder()
				.setUsername(username)
				.setPassword(password)
				.validate()
				.build();
	}
	
	public static User newUser(String username,
			String password,
			String completeName)
	{
		return builder()
				.setUsername(username)
				.setPassword(password)
				.setCompleteName(completeName)
				.validate()
				.build();
	}
	
	public static User copyUser(User user)
	{
		return builder()
				.setId(user.getId())
				.setVersion(user.getVersion())
				.setUsername(user.getUsername())
				.setPassword(user.getPassword())
				.setCompleteName(user.getCompleteName())
				.setActive(user.isActive())
				.validate()
				.build();
	}
	
	public static Builder builder()
	{
		return new Builder();
	}
	
	public static class Builder
	{
		private Long id;
		private Long version;
		
		private String username;
		private String password;
		private String completeName;
		
		private Boolean active;
		
		private Date lastUpdated;
		
		private Builder()
		{
			
		}
		
		public Builder setId(Long id)
		{
			this.id = id;
			return this;
		}
		
		public Builder setVersion(Long version)
		{
			this.version = version;
			return this;
		}
		
		public Builder setUsername(String username) 
		{
			this.username = username;
			return this;
		}

		public Builder setPassword(String password) 
		{
			this.password = password;
			return this;
		}

		public Builder setCompleteName(String completeName) 
		{
			this.completeName = completeName;
			return this;
		}

		public Builder setActive(Boolean active) 
		{
			this.active = active;
			return this;
		}

		public Builder setLastUpdated(Date lastUpdated)
		{
			this.lastUpdated = lastUpdated;
			return this;
		}
		
		public User build()
		{
			User user = new User();
		
			user.setId(id);
			user.setVersion(version);
			
			user.setUsername(username);
			user.setPassword(password);
			user.setCompleteName(completeName);
			user.setActive(active!=null ? active : Boolean.TRUE);
			
			Date currentDate = new Date();
			user.setLastUpdated(lastUpdated!=null ? lastUpdated : currentDate);
			
			return user;
		}
		
		public Builder validate()
		{
			List<TypedConstraintViolation> errors = new ArrayList<>();
			if(StringUtils.isBlank(username)){
				// [CORE-1001] No s'ha especificat el nom d'usuari
				errors.add(new TypedConstraintViolation(CoreErrorConstants.ERROR_CODE_CORE_1001, CoreErrorConstants.ERROR_DESC_CORE_1001));
			}
			if(StringUtils.isBlank(password)){
				// [CORE-1002] No s'ha especificat el password de l'usuari
				errors.add(new TypedConstraintViolation(CoreErrorConstants.ERROR_CODE_CORE_1002, CoreErrorConstants.ERROR_DESC_CORE_1002));
			}
			
			if(!errors.isEmpty()){
				throw new TypedConstraintViolationException()
						.setConstraintViolations(errors);
			}
			
			return this;
		}
	}
}
