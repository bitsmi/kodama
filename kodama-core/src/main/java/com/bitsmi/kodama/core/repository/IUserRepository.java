package com.bitsmi.kodama.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.bitsmi.kodama.core.domain.User;

public interface IUserRepository  extends JpaRepository<User, Long>  
{
	public User findById(Long id);
}