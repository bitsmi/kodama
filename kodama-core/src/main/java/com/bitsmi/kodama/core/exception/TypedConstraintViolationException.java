package com.bitsmi.kodama.core.exception;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TypedConstraintViolationException extends RuntimeException 
{
	private static final long serialVersionUID = 8481685522301350284L;
	
	private List<TypedConstraintViolation> constraintViolations;
	
	public List<TypedConstraintViolation> getConstraintViolations()
	{
		if(constraintViolations==null){
			return Collections.emptyList();
		}
		
		return constraintViolations;
	}
	
	public TypedConstraintViolationException setConstraintViolations(List<TypedConstraintViolation> constraintViolations)
	{
		this.constraintViolations = constraintViolations;
		return this;
	}
	
	public TypedConstraintViolationException addConstraintViolation(String code, String message)
	{
		if(this.constraintViolations==null){
			this.constraintViolations = new ArrayList<>();
		}
		
		this.constraintViolations.add(new TypedConstraintViolation(code, message));
		return this;
	}
	
	public TypedConstraintViolationException addConstraintViolations(List<TypedConstraintViolation> constraintViolations)
	{
		if(this.constraintViolations==null){
			this.constraintViolations = new ArrayList<>();
		}
		
		this.constraintViolations.addAll(constraintViolations);
		return this;
	}
	
	@Override
	public String getMessage()
	{
		StringBuilder messageBuilder = new StringBuilder();
		for(TypedConstraintViolation error:getConstraintViolations()){
			if(messageBuilder.length()>0){
				messageBuilder.append(", ");
			}
			messageBuilder.append(error.getMessage());
		}
		
		return messageBuilder.toString();
	}
}
