package com.bitsmi.kodama.core.dto;

import java.util.List;
import java.util.Map;

public class SqlResultsetDto implements ISqlResultDto 
{
	public static final String TYPE = "SQL_RESULTSET";
	
	private List<Map<String, Object>> results;
	
	public SqlResultsetDto(List<Map<String, Object>> results)
	{
		this.results = results;
	}
	
	@Override
	public String getType()
	{
		return TYPE;
	}
	
	public int size()
	{
		return results.size();
	}
	
	public List<Map<String, Object>> getResults()
	{
		return this.results;
	}
}
