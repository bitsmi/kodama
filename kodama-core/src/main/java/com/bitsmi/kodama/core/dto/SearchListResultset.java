package com.bitsmi.kodama.core.dto;

import java.util.Collections;
import java.util.List;

public class SearchListResultset <T> implements ISearchResultset<T> 
{
	private List<T> results;
	private int page;
	private int pageSize;
	private long totalElements;
	
	public SearchListResultset(List<T> results, int page, int pageSize, long totalElements)
	{
		this.results = results;
		this.page = page;
		this.pageSize = pageSize;
		this.totalElements = totalElements;
	}
	
	@Override
	public List<T> getResults() 
	{
		return results;
	}

	@Override
	public long getTotalElements() 
	{
		return totalElements;
	}

	@Override
	public int getPage() 
	{
		return page;
	}

	@Override
	public int getPageSize() 
	{
		return pageSize;
	}
	
	public static <T> SearchListResultset<T> emptyResultSet()
	{
		return new SearchListResultset(Collections.emptyList(), 0, 0, 0);
	}
}
