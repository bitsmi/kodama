package com.bitsmi.kodama.core.domain.factory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import com.bitsmi.kodama.core.CoreErrorConstants;
import com.bitsmi.kodama.core.domain.Parameter;
import com.bitsmi.kodama.core.exception.TypedConstraintViolation;
import com.bitsmi.kodama.core.exception.TypedConstraintViolationException;

public class ParameterFactory 
{
	public static Parameter newParameter(String key,
			String value)
	{
		return builder()
				.setParamKey(key)
				.setParamValue(value)
				.validate()
				.build();
	}
	
	public static Parameter copyIssue(Parameter parameter)
	{
		return builder()
				.setId(parameter.getId())
				.setVersion(parameter.getVersion())
				.setParamKey(parameter.getParamKey())
				.setParamValue(parameter.getParamValue())
				.validate()
				.build();
	}
	
	public static Builder builder()
	{
		return new Builder();
	}
	
	public static class Builder
	{
		private Long id;
		private Long version;
		
		private String key;
		private String value;
		
		private Date lastUpdated;
		
		private Builder()
		{
			
		}
		
		public Builder setId(Long id)
		{
			this.id = id;
			return this;
		}
		
		public Builder setVersion(Long version)
		{
			this.version = version;
			return this;
		}
		
		public Builder setParamKey(String key) 
		{
			this.key = key;
			return this;
		}

		public Builder setParamValue(String value) 
		{
			this.value = value;
			return this;
		}
		
		public Builder setLastUpdated(Date lastUpdated)
		{
			this.lastUpdated = lastUpdated;
			return this;
		}
		
		public Parameter build()
		{
			Parameter issue = new Parameter();
		
			issue.setId(id);
			issue.setVersion(version);
			
			issue.setParamKey(key);
			issue.setParamValue(value);
			
			Date currentDate = new Date();
			issue.setLastUpdated(lastUpdated!=null ? lastUpdated : currentDate);
			
			return issue;
		}
		
		public Builder validate()
		{
			List<TypedConstraintViolation> errors = new ArrayList<>();
			if(StringUtils.isBlank(key)){
				// [CORE-0002] No s'ha especificat el codi del paràmetre 
				errors.add(new TypedConstraintViolation(CoreErrorConstants.ERROR_CODE_CORE_0002, CoreErrorConstants.ERROR_DESC_CORE_0002));
			}
			if(StringUtils.isBlank(value)){
				// [CORE-0003] No s'ha especificat el valor associat al paràmetre 
				errors.add(new TypedConstraintViolation(CoreErrorConstants.ERROR_CODE_CORE_0003, CoreErrorConstants.ERROR_DESC_CORE_0003));
			}
			
			if(!errors.isEmpty()){
				throw new TypedConstraintViolationException()
						.setConstraintViolations(errors);
			}
			
			return this;
		}
	}
}
