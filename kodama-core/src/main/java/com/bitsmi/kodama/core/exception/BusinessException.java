package com.bitsmi.kodama.core.exception;

import com.bitsmi.kodama.spi.exception.TypedException;

public class BusinessException extends TypedException
{
	private static final long serialVersionUID = -8543041899695242262L;

	public BusinessException(String errorCode) 
	{
		super(errorCode);
	}

	public BusinessException(String errorCode, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) 
	{
		super(errorCode, message, cause, enableSuppression, writableStackTrace);
	}

	public BusinessException(String errorCode, String message, Throwable cause) 
	{
		super(errorCode, message, cause);
	}

	public BusinessException(String errorCode, String message) 
	{
		super(errorCode, message);
	}

	public BusinessException(String errorCode, Throwable cause) 
	{
		super(errorCode, cause);
	}
}
