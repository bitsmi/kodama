package com.bitsmi.kodama.core;

public class CoreErrorConstants 
{
	/* GENERICS */
	/** [CORE-0001] S'ha produit un error no especificat: &lt;Missatge d'error&gt; */
	public static final String ERROR_CODE_CORE_0001 = "CORE-0001";
	public static final String ERROR_DESC_CORE_0001 = "S'ha produit un error no especificat: %1$s";
	
	/** [CORE-0002] No s'ha especificat el codi del paràmetre */
	public static final String ERROR_CODE_CORE_0002 = "CORE-0002";
	public static final String ERROR_DESC_CORE_0002 = "No s'ha especificat el codi del paràmetre";
	
	/** [CORE-0003] No s'ha especificat el valor associat al paràmetre */
	public static final String ERROR_CODE_CORE_0003 = "CORE-0003";
	public static final String ERROR_DESC_CORE_0003 = "No s'ha especificat el codi del paràmetre";
	
	/** [CORE-0004] El format del paràmetre (&lt;codi paràmetre&gt;) no és vàlid [&lt;patró paràmetre&gt;] */
	public static final String ERROR_CODE_CORE_0004 = "CORE-0004";
	public static final String ERROR_DESC_CORE_0004 = "El format del paràmetre (%1$s) no és vàlid [%2$s]";
	
	/* USER */
	/** [CORE-1001] No s'ha especificat el nom d'usuari */
	public static final String ERROR_CODE_CORE_1001 = "CORE-1001";
	public static final String ERROR_DESC_CORE_1001 = "No s'ha especificat el nom d'usuari";
	
	/** [CORE-1002] No s'ha especificat el password de l'usuari */
	public static final String ERROR_CODE_CORE_1002 = "CORE-1002";
	public static final String ERROR_DESC_CORE_1002 = "No s'ha especificat el password de l'usuari";
	
	/* INTERFICIE SQL */
	/** [CORE-9001] La sentencia especificada no és vàlida [&lt;nom job&gt;] */
	public static final String ERROR_CODE_CORE_9001 = "CORE-9001";
	public static final String ERROR_DESC_CORE_9001 = "La sentencia especificada no és vàlida [%1$s]";
}
