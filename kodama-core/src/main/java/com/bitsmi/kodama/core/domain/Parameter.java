package com.bitsmi.kodama.core.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import org.hibernate.annotations.Type;

/**
 * TODO v1.0 javadoc
 * @author Antonio Archilla Nava
 * @version 0.1.0
 */
@SequenceGenerator(name="PARAMETER_ID_GENERATOR", 
		sequenceName="SEQ_PARAMETER",
		allocationSize=1)
@Entity
public class Parameter implements Serializable 
{
	private static final long serialVersionUID = 2486964099187776123L;

	@Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="PARAMETER_ID_GENERATOR")
	protected Long id;
    
    @Version
    protected Long version;
    
    protected String paramKey;
    
    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(length=1000)
    protected String paramValue;
    
    /**
     * Data de l'última actualització del registre a BDD
     */
    @Temporal(TemporalType.TIMESTAMP)
    protected Date lastUpdated;
    
    public Long getId() 
	{
		return id;
	}
	
	public void setId(Long id) 
	{
		this.id = id;
	}
    
    public Long getVersion()
    {
        return version;
    }
    
    public void setVersion(Long version)
    {
        this.version = version;
    }
    
    public String getParamKey() 
    {
		return paramKey;
	}

	public void setParamKey(String paramKey) 
	{
		this.paramKey = paramKey;
	}

	public String getParamValue() 
	{
		return paramValue;
	}

	public void setParamValue(String paramValue) 
	{
		this.paramValue = paramValue;
	}

	public Date getLastUpdated() 
    {
        return lastUpdated;
    }

    public void setLastUpdated(Date lastUpdated) 
    {
        this.lastUpdated = lastUpdated;
    }
    
    /**
     * Callback executat abans de realitzar l'inserció / actualització del registre. 
     * Servirà per actualitzar la data de creació en cas d'inserció i el timestamp en cas d'actualització
     */
    @PreUpdate
    @PrePersist
    public void updateDefaultValues()
    {
        lastUpdated = new Date();     
    }
    
    @Override
    public int hashCode() 
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((paramKey == null) ? 0 : paramKey.hashCode());
		
        return result;
    }
	
    @Override
    public boolean equals(Object obj) 
    {
        if(this==obj){
            return true;
        }
        if(obj==null){
            return false;
        }
        if(getClass()!=obj.getClass()){
            return false;
        }
		
        Parameter other = (Parameter) obj;
        /* Key */
        if(paramKey==null){
            if(other.paramKey!=null){
                return false;
            }
        } 
        else if(!id.equals(other.id)){
            return false;
        }
        
        return true;
    }
}
