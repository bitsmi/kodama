package com.bitsmi.kodama.core.repository;

import java.util.List;
import java.util.Map;

public interface IRawSqlRepository 
{
	public List<Map<String, Object>> select(String statement);
	public Number insert(String statement);
	public void update(String statement);
	public void delete(String statement);
	public void executeStatement(String statement);
}
