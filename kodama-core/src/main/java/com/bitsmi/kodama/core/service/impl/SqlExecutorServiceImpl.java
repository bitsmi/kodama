package com.bitsmi.kodama.core.service.impl;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.bitsmi.kodama.core.CoreErrorConstants;
import com.bitsmi.kodama.core.dto.ISqlResultDto;
import com.bitsmi.kodama.core.dto.SqlMessageDto;
import com.bitsmi.kodama.core.dto.SqlResultsetDto;
import com.bitsmi.kodama.core.exception.BusinessException;
import com.bitsmi.kodama.core.repository.IRawSqlRepository;
import com.bitsmi.kodama.core.service.ISqlExecutorService;

@Service
public class SqlExecutorServiceImpl implements ISqlExecutorService 
{
	@Autowired
	private IRawSqlRepository sqlRepository;
	
	public void setSqlRepository(IRawSqlRepository sqlRepository)
	{
		this.sqlRepository = sqlRepository;
	}
	
	/**
	 * TODO El reconeixement de les sentències és molt bàsic. S'haurà d'afegir una capa més (Parser)
	 * @param statement
	 */
	@Override
	public ISqlResultDto executeStatement(String statement) throws BusinessException
	{
		String testStatement = statement.toUpperCase(Locale.ROOT);
		try{
			ISqlResultDto resultDto = null;
			if(testStatement.startsWith("SELECT")){
				List<Map<String, Object>> results = sqlRepository.select(statement);
				resultDto = new SqlResultsetDto(results);
			}
			else if(testStatement.startsWith("INSERT")){
				sqlRepository.insert(statement);
				resultDto = new SqlMessageDto("INSERT executat");
			}
			else if(testStatement.startsWith("UPDATE")){
				sqlRepository.update(statement);
				resultDto = new SqlMessageDto("UPDATE executat");
			}
			else if(testStatement.startsWith("DELETE")){
				sqlRepository.delete(statement);
				resultDto = new SqlMessageDto("DELETE executat");
			}
			else{
				sqlRepository.executeStatement(statement);
				resultDto = new SqlMessageDto("Comanda executada");
			}
			
			return resultDto;
		}
		catch(Exception e){
			// [CORE-9001] La sentencia especificada no és vàlida [<sentencia>] 
			String errMsg = String.format(CoreErrorConstants.ERROR_DESC_CORE_9001, statement);
			throw new BusinessException(CoreErrorConstants.ERROR_CODE_CORE_9001, errMsg);
		}
	}
}
