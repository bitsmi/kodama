package com.bitsmi.kodama.core.dto;

public interface ISqlResultDto 
{
	public String getType();
}
