package com.bitsmi.kodama.core.dto;

import java.util.List;

public interface ISearchResultset <T> 
{
	public List<T> getResults();
	public long getTotalElements();
	public int getPage();
	public int getPageSize();
}
