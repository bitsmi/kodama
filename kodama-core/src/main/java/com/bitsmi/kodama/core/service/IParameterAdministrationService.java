package com.bitsmi.kodama.core.service;

import com.bitsmi.kodama.core.domain.Parameter;

public interface IParameterAdministrationService 
{
	public Parameter findParameterByKey(String key);
	public String findParameterValueByKey(String key);
	public String findParameterValueByKey(String key, String defaultValue);
	public void saveParameter(String key, String value);
	public void deleteParameter(String key);
}
