package com.bitsmi.kodama.core.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.bitsmi.kodama.core.repository.IRawSqlRepository;

@Repository
public class RawSqlRepositoryImpl implements IRawSqlRepository 
{
	private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) 
    {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    @Override
    public Number insert(final String statement)
    {
    	KeyHolder keyHolder = new GeneratedKeyHolder();
    	this.jdbcTemplate.update(new PreparedStatementCreator() 
    	{
    		@Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException 
            {
                PreparedStatement ps = connection.prepareStatement(statement);
                
                return ps;
            }
        }, keyHolder);
    	
    	return keyHolder.getKey();
    }
    
    @Override
    public List<Map<String, Object>> select(String statement)
    {
    	return this.jdbcTemplate.queryForList(statement, (Object[])null);
    }
    
    @Override
    public void update(String statement)
    {
    	executeStatement(statement);
    }
    
    @Override
    public void delete(String statement)
    {
    	executeStatement(statement);
    }
    
    @Override
    public void executeStatement(String statement)
    {
    	this.jdbcTemplate.execute(statement);
    }
}
